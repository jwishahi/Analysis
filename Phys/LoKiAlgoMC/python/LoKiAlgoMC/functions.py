#!/usr/bin/env python
# =============================================================================
## @file decorators.py LoKiAlgoMC/decorators.py
#  The set of basic decorator for objects from LoKiAlgoMC library
#  The file is a part of LoKi and Bender projects
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
# =============================================================================
"""
The set of basic decorators for objects from LoKiAlgoMC library
"""
__author__ = "Vanya BELYAEV ibelyaev@physics.syr.edu" 
# =============================================================================

# =============================================================================
# The END 
# =============================================================================
